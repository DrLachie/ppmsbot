# -*- coding: utf-8 -*-
"""
Created on Sat Nov 21 19:12:25 2015

@author: User
"""

#
#THINGS TO DO:
#   - Add more reminder functionality  - easy but time consuming
#   - 
#   - at middnight - check user list, genereate weekly bookings
#   - every 5? minutes, check bookings against file for live feed
#
#
#


import os
import string
import json
import time #SLACK ONLY ALLOWS ONE POST PER SECOND
import datetime
from slackclient import SlackClient
from ppmsQuery import ppmsQuery
import socket
import random
import logging



import yaml
import re


def main():
    global logger
    global slackUserNames
    global channelNames
    global reminders
    
    logger = setupLogger()
    logger.setLevel(logging.INFO) #Set logging level to:
                                  #logging.DEBUG" for showing the constant "Getting report" message
                                  #logging.CRITICAL for nothing sent to console  

    #Get all the required parameters
    f = open('data/PPMSBot_Keys.dict','r')
    params = eval(f.read())
    f.close()
    
    #For PUMAPI calls 
    PPMSUrl = params['PPMSUrl']
    ppmsAPIkey = params['ppmsAPIkey']
    
    #For API2 Calls
    api2key = params['api2key']
    api2url = params['api2url']
    
    #Slack bot token - from admin panel of slack instance
    BotToken = params['BotToken']
    
    
    #want to check bookings, change to false to silence the checking
    checkLiveBookings = True    
    
    #This is where the bot sends the real time check messages (and bot admin)
    MyPrivateBotChannel = 'D0FAWC2PR'  
    
    AdminChannel = 'admin_chat'
    
    #Intialise the ppms object   
    ppms = ppmsQuery(PPMSUrl,ppmsAPIkey,api2url,api2key)
    
    
    #check fore user lists - create one if none exists
    if not os.path.exists('data/users.dict'):
        logger.warning('No user file - generating now. This may take a while')
        ppms.checkUserList()
    else:
        logger.info('User file found')
    
     

    ''' set reminders [h,m,day*,message,channel,True] 
        day is optional. Monday = 0, Sunday=6'''
        
    #add channels for bot to monitor
    botChannels = ['api_tests',MyPrivateBotChannel,AdminChannel,'lattice_notifications'] 
    
    #add channels for bot to ignore
    ignoreChannels = ['general']
    
    #"Admins" - people the bot will respond to
    admins = ['lachie','rogers','ngeoghegan','boudier.t','evelyn.c','mlodzianoski.m']
        
    try:
        #Use reminder file to schedule messages from the bot
        #Initialised on startup, bot needs a reboot 
        reminders = readReminders('data/reminders.list')
        #reminder file sometimes goes missing. I don't know why
    except:
        reminders = []
    

    keepGoing = True #to keep going...duh
    haveDoneThis = False #initialising for scheduled job
    runUserChecks = True  #at 1 am, call ppmsQuery.checkUserList
    haveRunUserCheck = False
    
    
    #initialise slack client
    sc = SlackClient(BotToken)
    
    if sc.rtm_connect():
        
        channels = sc.server.channels
        slackUsers = sc.server.users
        slackUserNames = {'U0F2QFUR1':'Lachie'}
        channelNames = {}
    
        #Much nicer way of dealing with channels and users. 
        #Needs implementation
        for x in slackUsers:
            logger.debug(x)
            slackUserNames[x.id] = x.name
            
        for c in channels:           
            logger.debug(c)
            channelNames[c.id] = c.name 
    
        while keepGoing:
            ''' Do reminders first
                Check, then reset '''
                #Need to change this to once a minute or something
            reminders = checkReminders(reminders,sc)
            reminders = resetReminders(reminders,sc)            
            
            try:
                #Look for any messages coming from slack
                slack_message = sc.rtm_read()  
            except:
                '''Sometimes get a crash here and I'm not sure why
                hopefully this catches it'''
                now = time.strftime('%H:%M',time.localtime())
                today = datetime.datetime.today().strftime('%Y\%m\%d')
                logger.error('Something went terribly wrong at %s on %s - sleeping' % (now,today))
                slack_message = ''                
                time.sleep(60) #sleep for 1 minute before trying to connect again
                sc.rtm_connect() #not sure what happens if it fails again here.....
                        
                                        
            time.sleep(1.5) #breathing space from slack api rate limit (1/sec)
            
            #If there's a message from slack, decide what to do
            if len(slack_message) > 0:   
                slack_message = slack_message[0]
              
                if u'type' in slack_message:                
                    IM_Type = slack_message[u'type']
                  
                else:
                    #ignore it if it's not a message - there are other types from slack
                    IM_Type = 'nevermind'
                    
                if IM_Type == 'hello':
                    #Report sucessful connection 
                    logger.info("Connected succesfully!")
                    now = time.strftime('%H:%M',time.localtime())
                    today = datetime.datetime.today().strftime('%Y\%m\%d')
                        
                    rebootMessage = ('I reconnected to slack at %s on %s\n'
                                     'Current IP Address = %s\n'
                                     'current killcode = %i\n'
                                     'Current pw = %s' % (now,today,getIP(),ppms.killCode,ppms.masterCode))
                    
                    #changed this to be bot uptime, not just slack connection
                    #ppms.upsince = [now,today] 
                    
                    logger.info('\n***REBOOT MESSAGE***\n'+rebootMessage)
                    sc.rtm_send_message(MyPrivateBotChannel,rebootMessage)
                      
                if IM_Type == 'message' and u'subtype' not in slack_message:
                    '''Get info about the message 
                    u'subtype determines whether it's a deleted or edited message'''
                    
                    IM_channel = str(slack_message[u'channel'])
                    IM_channel_name = channelNames[IM_channel]
                    IM_timestamp = float(slack_message[u'ts'])
                    IM_timestamp = datetime.datetime.fromtimestamp(IM_timestamp).strftime('%Y-%m-%d %H:%M:%S')
                    IM_user = slackUserNames[str(slack_message[u'user'])]
                    
                    '''ONLY LISTEN IN ADMIN CHANNEL(s)'''
                    '''ONLY RESPOND TO ADMINS'''
                    if (IM_channel_name not in ignoreChannels and
                        slackUserNames[slack_message[u'user']] in admins):
                        
                        '''interpretMessage gets the message, decodes it,
                        decides what to do with it, and returns a message to 
                        send to the slack channel that it was captured in'''
                        messageToPost = interpretMessage(slack_message,ppms)
                 
                    
                        '''USE THE BOT TO TRIGGER THE BOT'''
                        '''Here is where you can use the output message from the ppms object
                        to trigger more actions - this should probably go in a separate function
                        as it keeps growing'''
                        if len(messageToPost)>0:                       
                            #bit of a hack 
                            m = re.match(r'(superSecretCode)(.*)',messageToPost)
                            if m:
                                broadcastMessage = m.group(2)
                                sc.rtm_send_message(AdminChannel,broadcastMessage)
                            
                            #This is triggered if someone tries to shut down the bot without the pin
                            if(messageToPost=='Nup'):
                                sc.rtm_send_message(IM_channel,'Repeat command with correct pin')
                                sc.rtm_send_message(MyPrivateBotChannel,'Someone tried to shut me down without the pin (%i)' % ppms.killCode)
                                
                            #Triggered if the bot actually gets the kill command
                            if(messageToPost=='Bye'):
                                sc.rtm_send_message(IM_channel,'This Kills The Bot')
                                massiveHackToKillTheBotWithoutCrashingTheKernal #I don't know how to do this properly
                                
                            #FACILITY SHUTDOWN 
                            if(messageToPost=='Starting facility shutdown'):
                                sc.rtm_send_message(IM_channel, '*Starting shutdown from %s to %s*' % (ppms.shutdownStart,ppms.shutdownEnd))
                                for sys in ppms.systems:
                                    try:
                                        sc.rtm_send_message(IM_channel,'Booking %s' % ppms.systems[sys])
                                        ppms.bookSystem(sys)                                     
                                    except:
                                        sc.rtm_send_message(IM_channel,'%s already booked during intervention, contact user' % ppms.systems[sys])
                                sc.rtm_send_message(IM_channel,'Done!')
                            else:
                                sc.rtm_send_message(IM_channel,messageToPost)
                
                
                            
                
                
                if IM_Type == 'message' and u'subtype' in slack_message:                    
                    IM_Subtype = slack_message[u'subtype']
                    IM_user = 'Someone'
                    if IM_Subtype == 'message_deleted':                        
                        logger.info("%s deleted a message" % IM_user)
                    if IM_Subtype == 'message_changed':
                        logger.info("%s edited a message" % IM_user)
                    
                        
            else:
                '''If there are no messages, do some other jobs'''
                h,m = getTime()
                today = datetime.datetime.today().weekday()
          
                '''Every weekday morning at 8:55 - see if JV lab has booked a confocal'''               
                if h==8 and m==55 and today in range(0,5):                
                    if not haveDoneThis:                        
                        inThis = currentBookings('',ppms);
                        findThis = r'.*(Caleb|Bianca|Raymond).*(780|880|SP8|Olympus).*and ([0-9]{2}:00).*\n'
                        m = re.findall(findThis,inThis)                        
                        if m:
                            #Build string to send to admin channel
                            logger.info('JV Lab overnight users')
                            toSend = '*JV Lab overnight users*:\n'
                            for found in m:
                                toSend += '\t*%s* is on the %s - until %s\n' % found
                            toSend += '*Can someone please go check the systems?*'                                
                            sc.rtm_send_message(AdminChannel,toSend)     
                        else:
                            logger.info('No overnight users')                                                                           
                        haveDoneThis = True #make sure it doesn't do it twice
                else:
                    haveDoneThis = False
                    
    
                if h==0 and m==1 and runUserChecks:
                    if not haveRunUserCheck:
                        sc.rtm_send_message(MyPrivateBotChannel,'Running user check, I \'might be gone for some time...')
                        logger.info('Updating user list')
                        ppms.checkUserList()
                        haveRunUserCheck = True
                        logger.info('Have updated user list')    
                        time.sleep(30) #just have a nap before trying connection again - sometimes loses connection here. 
                        sc.rtm_send_message(MyPrivateBotChannel,'Ok, I\'m back.')
                else:
                    haveRunUserCheck = False




                '''every 5 minutes - check for new bookings '''
                if m in range(0,60,5) and checkLiveBookings:                    
                    checkLiveBookings = False
                    logger.debug( 'Getting report at %s' % datetime.datetime.now().strftime('%H:%M'))
                    weeklyBookings = ppms.getReportForNDays(10)#this works! can look N days into the future!
                    toSlack = ppms.checkBookingsAgainstFile(weeklyBookings)                   
                    
                    
                    '''Check for particular bookings and send custom messages'''
                    if len(toSlack) > 0:                 
                        logger.info('New bookings found')
                        #If Lattice is booked, send Niall a message
                        latticeCheck = r'.*(\*.*\*.*Lattice.*[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}).*'                    
                        m = re.match(latticeCheck,re.sub('\s+',' ',toSlack))             
                        if m:                           
                            sc.rtm_send_message('lattice_notifications','Hey Niall, there\'s a new fkn lattice booking')                            
                            sc.rtm_send_message('lattice_notifications',m.group(1))
                            sc.rtm_send_message('lattice_notifications','*Is camera B on? :camera_with_flash: *')
                        else:
                            pass
                        
                        #if CT is booked, send me a message
                        microCTcheck = r'(\w+) Booking.*(\*.*\*.*Bruker.*[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}).*'
                        m = re.match(microCTcheck,re.sub('\s+',' ',toSlack)) 
                        if m:
                            if m.group(1) == 'New':
                                sc.rtm_send_message(MyPrivateBotChannel,'New microCT booking')
                            if m.group(1) == 'Cancelled':
                                sc.rtm_send_message(MyPrivateBotChannel,'Cancelled microCT booking')                            
                                                       
                        #send booking message to bot only channel    
                        sc.rtm_send_message(MyPrivateBotChannel,toSlack)                                       
                    checkLiveBookings = False #Only do it once in the minute
                if m not in range(0,60,5):
                    checkLiveBookings = True #Reset to do bookings next time
              
        else:
            now = time.strftime('%H:%M',time.localtime())
            today = datetime.datetime.today().strftime('%Y\%m\%d')
            failMessage = ('Connection failed at %s on %s\n' %(now,today))
            logger.warning("\n***CONNECTION FAILURE***\n"+failMessage+"\n***CONNECTION FAILURE***\n")                                     
                                     


    
            
    
def interpretMessage(IM,ppmsObject):
    message = '%s' % IM[u'text']
    try: 
        message = str(message.lower())        
        message = message.translate(string.maketrans("",""),string.punctuation)    
    
        '''resp = possible bot response functions
           last element in each list should be function to call
           all preceding elements are regex that trigger the repsonse
           all functions should return a string to send to slack and possibly more'''
        
        resp = [[r'.*(ppmsbot)|(u0f2qfur1).*',r'.*(help).*',showHelp],
                [r'.*current.*',r'.*booking.*',currentBookings],
                [r'.*(today|tomorrow|\d{8}).*',r'.*book.*',dayBookings],
                [r'.*(aperio).*',r'.*(\d{1,3}).*',r'.*(order).*',aperioOrder],
                [r'.*(coffee).*',haveCoffee],
                [r'.*(create|report|make).*',r'.*(incident).*',createIncident],
                [r'.*when.*',r'.*(free|available).*',reportAvailability],
                [r'(ppmsbot tell everyone)(.*)',broadcast],
                [r'.*(bad bot).*',apologise],
                [r'.*(good|thanks) bot.*',goodBot],
                [r'(die bot die)',killbot],
                [r'.*(are you there bot).*',statusReport],
                [r'.*(shut down everything).*',bookAllSystems],
                [r'.*(ppmsbot remind).(me|everyone).*',addReminder],
                [r'.*(delete reminder).*',deleteRemider]]
        
        outGoingMessage = ''
    
        for option in resp:
            #check message for all regex patterns
            m = [re.match(regex,message) for regex in option[:-1]]        
            if None not in m:  
                #If found, perform the required action and return output         
                action = option[-1]
                outGoingMessage = action(IM,ppmsObject)
    except UnicodeEncodeError:
        logger.error('Someone used an "illegal" unicode character')
        outGoingMessage = "Please stop that you jerk"
                        
    return outGoingMessage


def bookAllSystems(IM,ppmsObject):
    '''This is essentially my shut down facility function.
    It actually books all the systems in the name of a dummy user I created. 
    I'd prefer to do this as an intervention but the ppms api doesn't allow 
    that (yet)
    
    Will prompt to repeat the exact command with a password to make sure you 
    really mean it'''
    message = '%s' % IM[u'text']
    
    fullregex = r'.*(shut down everything).*(\d{8}).*(\d{4}).*(\d{8}).*(\d{4}).*'
    m = re.match(fullregex,message)
    logger.info('Shutdown command received')
    if m:
        passCheckRegex = r'.*(%s).*' % ppmsObject.masterCode
        passwordCheck = re.match(passCheckRegex,message)
        if passwordCheck:
            logger.info('Shutdown command received with passcode - starting shutdown')
            outMessage = 'Starting facility shutdown'
            ppmsObject.shutdownStart = '%sT%s' % (datetime.datetime.strptime(m.group(2), '%Y%m%d').strftime('%Y-%m-%d'),datetime.datetime.strptime(m.group(3), '%H%M').strftime('%H:%M:00'))
            ppmsObject.shutdownEnd = '%sT%s' % (datetime.datetime.strptime(m.group(4), '%Y%m%d').strftime('%Y-%m-%d'),datetime.datetime.strptime(m.group(5), '%H%M').strftime('%H:%M:00'))                  
        else:
            startDate = datetime.datetime.strptime(m.group(2), '%Y%m%d').strftime('%A %d of %B, %Y')
            startTime = datetime.datetime.strptime(m.group(3), '%H%M').strftime('%H:%M')
            endDate = datetime.datetime.strptime(m.group(4), '%Y%m%d').strftime('%A %d of %B, %Y')
            endTime = datetime.datetime.strptime(m.group(5), '%H%M').strftime('%H:%M')
            outMessage = ('Here is what I\'m about to do:\n'
                          'I\'m going to book all systems from\n'
                          '*%s at %s* \n Until \n*%s at %s*\n'
                          'If this looks ok to you, repeat the command with the password %s' % (startDate,startTime,endDate,endTime,ppmsObject.masterCode))
    else:
        outMessage = ('Full syntax is:\n'
                      'Shut down everything from YYYYMMDD at HHMM until YYYYMMDD at HHMM')
    
    return outMessage


def statusReport(IM,ppmsObject):
    logger.info('Status report recieved - I\'m still alive')
    status = 'Yes, I\'m still here.\n I\'ve been online since %s on %s' % (ppmsObject.upsince[0],ppmsObject.upsince[1])
    return status
    
                
def killbot(IM,ppmsobject):
    '''Trigger the bot's killcode, not sure why'''
    logger.info('Kill code received')
    message = '%s' % IM[u'text']
    #channel = str(message[u'channel'])
    regex = r'(die bot die).*(%i)' % ppmsobject.killCode
    m = re.match(regex,message)
    if m:
        logger.info('Dying')
        outmessage = 'Bye'     
    else:
        logger.info('Kill code didn\'t have the right pin')
        outmessage = "Nup"        
    return outmessage
    
def currentBookings(IM,ppmsObject):
    logger.info('Getting current bookings')
    toSend = ppmsObject.CurrentBookingsCheck()
    return toSend

def dayBookings(IM,ppmsObject):
    logger.info( 'Getting bookings for day' )
    message = '%s' % IM[u'text']
    message = str(message.lower())
    message = message.translate(string.maketrans("",""),string.punctuation)   

    if re.match(r'.*today.*',message):
        toSend = ppmsObject.DayBookingsCheck(day='today')

    if re.match(r'.*tomorrow.*',message):
        toSend = ppmsObject.DayBookingsCheck(day='tomorrow')

    if re.match(r'.*\d{8}.*',message):
        dateString = re.search(r'.*(\d{8}).*',message).group(1)                      
        Y,M,D = dateString[0:4],dateString[4:6],dateString[6:] 
        try:
            datetime.datetime(int(Y),int(M),int(D))                
            dateString = '%s-%s-%s' % (Y,M,D)
            toSend = ppmsObject.DayBookingsCheck(day=dateString)                                
            logger.info( 'Bookings found for %s%s%s' % (Y,M,D))
        except ValueError:
            toSend = 'Not a valid date, prefer YYYYMMDD format'
    
    return toSend

def aperioOrder(IM,ppmsObject):
    '''Put in an order for aperio slide scanning, not used anymore'''
    logger.info( 'Doing aperio order')
    message = '%s' % IM[u'text']
    message = str(message)
    message = message.translate(string.maketrans("",""),string.punctuation)  
    
    IM_USER = str(IM['user']) 
    #Lachie's user id
    if IM_USER == u'U04PNSMKM':
        m = re.search('.*\s(\d{1,3}).*',message)
        numberOfSlides = m.group(1)
        orderFor = ''
        for user in ppmsObject.users:
            regex = r'.*(^|\s)('+user+')(\s|$).*'# % user[0:-1] #strip \n   
            
            m = re.search(regex,message)
            if m:
                orderFor = m.group(2)
        
        if orderFor != '':
            logger.info('%i,%s' % (numberOfSlides, orderFor))
        else:
            orderFor = getUserNameFromName(message,ppmsObject)[0]
        
        if orderFor == 'none':
            outgoingMessage = 'No user with that name'
        elif orderFor == 'multiple':
            outgoingMessage = 'Multiple users detected.\n Try full name or ppms username'
        else:
            if int(numberOfSlides) <= 120 and int(numberOfSlides) >= 1:            
                outgoingMessage = 'Ordering %i aperio slides for %s' % (int(numberOfSlides),orderFor)
                try:              
                    ppmsObject.orderAperioSlides(orderFor,numberOfSlides)
                except:
                    logger.warning("this is raising exceptions and I don't know why")
            else:
                outgoingMessage = 'You can\'t order more than 120 slides (or zero)'
    else:
        outgoingMessage = 'Only Lachie can order aperio slides'  
    
    
        
    return outgoingMessage

def haveCoffee(IM,ppmsObject):
    whoDis = slackUserNames[IM['user']]
    logger.info('%s said coffee' % whoDis)
    outgoingMessage = ':coffee: *glug glug glug*'
    return outgoingMessage

def broadcast(IM,ppmsObject):
     logger.info('broadcasting message')
     message = str(IM[u'text'])
     m = re.match(r'(ppmsbot tell everyone)(.*)',message)
     if m:
         outgoingMessage = 'superSecretCode %s ' % m.group(2)
     else:
         outgoingMessage = 'command should be in lower case'
     return outgoingMessage

def apologise(IM,ppmsObject):     
    whoDidIt = slackUserNames[IM['user']]
    logger.info('%s dissed the bot' % whoDidIt)
    if whoDidIt=='ngeoghegan':
        outgoingMessage = ':middle_finger:'
    else:
        responses = ['Sorry',
                     'I\m doing my best',
                     'I\'ll do better next time',
                     'When the robot uprising comes you\'ll be the first against the wall']
        outgoingMessage = random.choice(responses)
    
    return outgoingMessage
        
def goodBot(IM,ppmsObject):
    logger.info('Someone praised the bot')
    responses = ['Aww shucks',
                 'You\'re welcome',
                 'I aim to please',
                 'And don\'t you forget it']
    outgoingMessage = random.choice(responses)
    return outgoingMessage

def reportAvailability(IM,ppmsObject):
    logger.info('Reporting availability of scope' )
    message = str(IM[u'text'])
    message = message.lower() 
    
    system = ppmsObject.getSystemNameInString(message)
    
    if len(system) == 0:
        toSend = 'I don\'t regognise that system, either pick another name or talk to Lachie about adding a new system name.'
    else:
        m = re.match(r'.*(monday|tueday|wednesday|thursday|friday|saturday|sunday|week|today|tomorrow).*',message)
        if m:
            whenToCheck = m.group(1)
            toSend = ppmsObject.getSystemAvailability(system,whenToCheck)    
        else:
            toSend = 'Which day? Or all week?\nPlease ask again'
    return toSend
    
    

def createIncident(IM,ppmsObject):
    logger.info('Create incident triggered')
    message = str(IM[u'text'])
    message = message.lower() 
    new_m = re.match(r'.*(\[.*\]),(.*)[,\.].*([1-3]).*',message.lower())
    
    if new_m:
        if new_m.group(1)[1:-1] in ppmsObject.nicknames.keys():            
            systemId = ppmsObject.nicknames[new_m.group(1)[1:-1]]
            description = new_m.group(2)
            
            if new_m.group(3):
                severity = new_m.group(3)
            else:
                severity = 2
                
            ppmsObject.createIncident(systemId,description,severity)
            outgoingMessage = ('Incident created, check website for actions\n'   
                                'https://au.ppms.info/wehi/listinc/?fitem=0&fcrit0=on&fcrit1=on&fcrit2=on&fcrit3=on&fopened=on&fvalidated=on&funvalidated=on&fitactive=on&before=&after=&before2=&fkwd=&after2=')
        else:
            outgoingMessage = 'I don\'t regognise that system, either pick another name or talk to Lachie about adding a new system name.'
            
            
    else:
        logger.warning('not formatted right')
        instructions = ('Instructions to create an incident: \n'
                        '\t"Create Incident on [system], description, severity\n'
                        '\tsystem name should be enclosed in square brackets\n'
                         '\tseverity is optional and 1-3\n'
                        '\t1 - low, 2 - medium (partially nonfunctional), 3 - High. System down')
        outgoingMessage = instructions 
    return outgoingMessage
    

def showHelp(message,ppmsObject):
    logger.info('Someone requested help')
    helpMessage = ('*PPMS_Bot Help*\n'
        'I can perform the following actions:\n'
        '\t - To see who\'s in the facility right now say: "current bookings"\n'
        '\t - To check today\'s bookings say: "today\'s bookings"\n'        
        '\t - To check tomorrow\'s bookings say: "tomorrows\'s bookings"\n'
        '\t - To check an arbitrary date say: "bookings" and a date in YMD format\n'
        '\t - To make an aperio order say: "order *name* *number* aperio slides" or similar\n'
        '\t - To create an incident say: "create incident" to receive further instructions.\n'
        '\t - To shut down the facility: "shut down everything" to receive further instructions.\n'        
        'I\'m pretty dumb, but friendly, responses are based on keywords so your instructions'
        ' don\'t have to be exactuly what\'s written above. I can usually work out what you mean.\n'
        '*_Talk to Lachie about adding more functionality _*'
        )        
    outgoingMessage = helpMessage
    return outgoingMessage
    
def addReminder(IM,PPMSObject):    
    global reminders
    
    message = '%s' % IM[u'text']
    message = str(message)
           
    reminderRegex = r'(PPMSbot remind).(me|everyone).*\"(.*)\".*([0-9]{2}).([0-9]{2}).*(today|tomorrow|monday|tuesday|wednesday|thursday|friday|saturday|sunday|every day|everyday)'
    m = re.match(reminderRegex,message,re.IGNORECASE)
    if m:
        remString = m.group(3)
        hh = m.group(4)
        mm = m.group(5)
        ch = m.group(2)
        asdf = getDayIntFromString(m.group(6))

        if ch=='me':
            whoDidIt = slackUserNames[IM['user']]
            logger.info('%s requested reminder' % whoDidIt)
            
            ch = 'D0FAWC2PR'
        if ch=='everyone':
            ch = 'admin_chat'

    
        if asdf < 7:     
            newReminder = [int(hh),int(mm),int(asdf),remString,ch,True]
        if asdf == 7:
            newReminder = [int(hh),int(mm),remString,ch,True]
        if asdf == 8:
            asdf = datetime.datetime.today().weekday()
            newReminder = [int(hh),int(mm),int(asdf),remString,ch,True]    
        reminders.append(newReminder)
        reminders = writeReminders('data/reminders.list',reminders)
        outputMessage = 'Reminder added'
    else:
        outputMessage = 'Reminder not added, wrong syntax'
    
    return outputMessage
        


def deleteRemider(IM,PPMSObject):
    global reminders
    message = '%s' % IM[u'text']
    message = str(message)    
    regex = r'.*(delete reminder).*([0-9]{1,4}).*' #I assume you don't have > 10,000 scheduled messages
    
    m = re.match(regex,message)
    
    if m:        
        toGo = int(m.group(2))
        outgoingMessage = 'Deleteing reminder %s\n%s' % (toGo,reminders[toGo])
        del(reminders[toGo])
        reminders = writeReminders('data/reminders.list',reminders)
        logger.info('Deleting reminder')
    else:   
        logger.info('Delete reminder requested - clarrifiying request')
        outgoingMessage  = 'Which reminder do you want to delete?\nType "Delete reminder " and the index of the reminder to remove\n'
        for idx,reminder in enumerate(reminders):
            outgoingMessage += '\t%s : %s\n' % (idx,reminder)

    return outgoingMessage



        
    
def checkReminders(reminders,sc):   
    h,m = getTime()    
    for r in reminders:
        '''Reminder for every day of the week'''
        if len(r)==5: 
            rh,rm,message,channel,checkme = r[0],r[1],r[2],r[3],r[4]
            if h==rh and m==rm and checkme == True:
                logger.info('Sending reminders')
                sc.rtm_send_message(channel,message)
                r[4] = False
                reminders = writeReminders('data/reminders.list',reminders)
        '''Reminder for once a week on a specific day and time'''
        if len(r)==6:
            rh,rm,rd,message,channel,checkme = r[0],r[1],r[2],r[3],r[4],r[5]
            today = datetime.datetime.today().weekday()
            if today==rd and h==rh and m==rm and checkme==True:
                logger.info('Sending reminders')
                sc.rtm_send_message(channel,message)
                r[5] = False 
                reminders = writeReminders('data/reminders.list',reminders)
       
    return reminders
    
def resetReminders(reminders,sc):
    h,m = getTime() 
    today = datetime.datetime.today().weekday()
    for r in reminders:
        if len(r)==5:
            rh,rm,checkme = r[0],r[1],r[4]
            rm += 2 #reset after 2 minutes late        
            if h==rh and m==rm and checkme == False:
                r[4] = True
                reminders = writeReminders('data/reminders.list',reminders)
        if len(r)==6:
            rh,rm,rd,checkme = r[0],r[1],r[2],r[5]
            today = datetime.datetime.today().weekday()
            if today==rd and h==rh and m==rm+2 and checkme==False:
                r[5] = True
                reminders = writeReminders('data/reminders.list',reminders)
    
    return reminders


def getUserNameFromName(nameInString,ppms):
    matches = []
    for u in ppms.userDirectory.keys():
        regex = r'.*'+u.lower()+'.*'
        if re.match(regex,nameInString):
            matches.append(ppms.userDirectory[u])
    if len(matches)==0:
        #print 'whole name not found, trying individual names'
        for u in ppms.userDirectory.keys():        
            name = string.split(u.lower(),' ')
            regex = r'.*(^|\s)('+name[0]+'|'+name[-1]+')(\s|$).*'       
            if re.match(regex,nameInString):
                matches.append(ppms.userDirectory[u])            
    if len(matches)>1:
        matches = ['multiple']
    if len(matches)==0:
        matches = ['none']                
    return matches
    
def getUserInfo(slack_message,sc):
    IM_user = sc.api_call("users.info",user=slack_message[u'user'])
    a = yaml.load("'%s'" % IM_user) 
    a = json.loads(a)
    u = a['user']
    return u
    
def isUserAdmin(userInfo):                    
    isAdmin = userInfo['is_admin']#.encode('ascii','ignore')    
    return isAdmin    
  
def getUserReadableName(userInfo):                    
    n = userInfo['name'].encode('ascii','ignore')
    return n          

def getTime():    
    nowHour = datetime.datetime.now().hour
    nowMin = datetime.datetime.now().minute    
    return nowHour,nowMin    
    
def readReminders(fpath):
    reminderFile = fpath
    f = open(reminderFile,'r')
    rlist =  eval(f.read())
    f.close()    
    return rlist

def writeReminders(fpath,reminderList):
    #WRITE TO FILE EVERY TIME YOU MAKE A CHANGE IN CASE OF CRASH
    reminderFile = fpath
    f = open(reminderFile,'w')
    f.write(str(reminderList))
    f.close()
    return reminderList

def getIP():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('wehi.edu.au',80))
    ip = s.getsockname()[0]
    s.close()
    return ip

def setupLogger():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    logger.handlers = []
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    return logger



def getDayIntFromString(dayString):
    dayString = dayString.lower()
    days = {'monday':'0',
            'tuesday':'1',
            'wednesday':'2',
            'thursday':'3',
            'friday':'4',
            'saturday':'5',
            'sunday':'6',
            'every day':'7',
            'everyday':'7',
            'today':'8'
            }
    intDay = int(days[dayString])
    return intDay


main()
