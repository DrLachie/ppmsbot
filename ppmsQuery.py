# -*- coding: utf-8 -*-
"""
Created on Tue Nov 24 09:23:47 2015

@author: whitehead
"""
#Things to do:
# 
# 
# 
#
#

import os
import urllib, urllib2
import string
import time, datetime
import re
import random

class ppmsQuery:
    def __init__(self,ppmsURL,apiKey,api2url,api2key):
        
        self.ppms = ppmsURL
        self.apiKey = apiKey
        self.systems = self.getSystems()
        self.API2URL = api2url
        self.api2key = api2key
        self.systemIDs = self.reverseSystems(self.systems)        
        self.userDirectory = self.readFromFile('data/users.dict')  
        self.nicknames = self.readFromFile('data/systemNicknames.dict')
        self.systemNames = self.readFromFile('data/systemFullNames.dict')
        self.users = [user for user in self.userDirectory.values()]
        self.killCode = random.randint(1000,9999)
        self.masterCode = self.generateNewMasterCode()    
        self.shutdownStart = 'YYYY-MM-DDTHH:MM:SS'
        self.shutdownEnd = 'YYYY-MM-DDTHH:MM:SS'
        self.bookingsInFile = [] #initialise this unneccessary
        
        now = time.strftime('%H:%M',time.localtime())
        today = datetime.datetime.today().strftime('%Y\%m\%d')
        self.upsince = [now,today]
        
        
        
 
  
    def bookSystem(self,sysId):
        '''gets times and dates from global variables self.shutdownStart and self.shutdownEnd
        This is an api2 call - care required '''
        commands = {'action':'setSessionBooking',
                    'apikey':self.api2key,
                    'coreid':2,
                    'start':self.shutdownStart,
                    'end':self.shutdownEnd,
                    'systemid':sysId,
                    'projectid':0,
                    'SE1':'false',
                    'SE2':'false',     
                    'comment':'',
                    'repeat':0,
                    'user':572, #PPMS INTERVENTION USER
                    'userid':572,
                    'assisted':'false',
                    'assistant':0,
                    'formid':0,
                    'form':''
                    }
        
                
        #c = urllib.urlencode(data)
        data = urllib.urlencode(commands)
        
        r = urllib2.Request(self.API2URL,data)
        response = urllib2.urlopen(r)
        time.sleep(2)
       # print('pinging ppms, %s' % datetime.datetime.now().strftime('%H:%M:%S'))
        output = response.read()
        output = True
        
        return output
 
    def reverseSystems(self,sysdict):
        reverse_map = dict(reversed(item) for item in sysdict.items())
        return reverse_map
    
    def generateNewMasterCode(self):
        
        chars = string.letters
        pw = ''.join((random.choice(chars)) for x in range(8))
        
        return pw
        
    def readFromFile(self,fpath):
        if os.path.exists(fpath):
            dictFile = fpath
            f = open(dictFile,'r')
            newdict =  eval(f.read())
            f.close()    
        else:
            newdict = {}
        return newdict

    def checkUserList(self):
        if len(self.getUserList()) != len(self.userDirectory):
            rawUsers = self.getUserList()           
    
            rawUsers = [r[0:-1] for r in rawUsers] #strip special characters
            newUsers = [u for u in rawUsers if u not in self.userDirectory.values()]
    
            for newUserID in newUsers:                
                realName = self.getUsersRealName(newUserID)
                self.userDirectory[realName] = newUserID
                print("'%s' : '%s'" % (realName,newUserID))
                       
            f = open('data/users.dict','w')            
            f.write(str(self.userDirectory))
            f.close()
        
        newDir = self.readFromFile('data/users.dict')
        return newDir
    
    def getSystems(self):
        commands = {'action':'getsystems',
                    'apikey':self.apiKey}
    
        sysList = self.Action(commands)
        sysList= string.split(sysList,'\n')
        sysList = sysList[1:-1] #strip first and last lines
        
        systems={}    #make system dicitonary 
        for line in sysList:
            line = string.split(line,',')
            systems[line[1]] = line[3][1:-1]                  
        return systems
    
    def getXPonSystem(self,userId,scopeId):
        commands = {'action': 'getuserexp',
                    'login':userId,
                    'id':scopeId,
                    'apikey':self.apiKey}
        userExp = self.Action(commands)
        #print userExp
        try:
            userExp = str.split(str.split(userExp,'\n')[1],',')[2]
        except:
            userExp = 0 #returns blank if no usage, also weirdness for admins?
            
        
        return userExp
        
    
    def getUserList(self):
        commands = {'action':'getusers',
                    'active':True,
                    'apikey':self.apiKey}
        userlist = self.Action(commands)    
        userlist = string.split(userlist,'\n')
        return userlist[0:-1] #strip last blank line
    
    def getUsersRealName(self,userID):
        commands = {'action':'getUser',
                    'noheaders':'true',
                    'login':userID,
                    'apikey':self.apiKey}
        person = self.Action(commands)        
        person = string.split(person,',')
        person = person[2][1:-1] + ' ' + person[1][1:-1]
        return person
        
    def orderAperioSlides(self,userID,numberOfSlides):
        commands = {'action':'createorder',
                    'apikey':self.apiKey,
                    'login':userID,
                    'quantity':numberOfSlides,
                    'serviceid':'020001'}
                    #'accepted':'true'}
                    
        data = urllib.urlencode(commands)
        r = urllib2.Request(self.ppms,data)
        response = urllib2.urlopen(r)
        time.sleep(2)
        #print('pinging ppms, %s' % datetime.datetime.now().strftime('%H:%M:%S'))
        order = response.read()     
        realName = self.getUsersRealName(userID)

        output = '%i slides ordered for %s' % (numberOfSlides,realName)
        return output
        
    def createIncident(self,system,description,severity=2):               
        commands = {'action':'createinc',
                    'id':system,
                    'descr':description,
                    'severity':severity,
                    'apikey':self.apiKey
                    }
        try:
            self.Action(commands)
        except:
            print('PPMS creates an exception for reasons I don\'t understand')
                
    
    def getBooking(self,system):
        commands = {'action':'getbooking',
                    'id':system,
                    'apikey':self.apiKey}
        booking = self.Action(commands)
        booking = string.split(booking,'\r\n')
        booking_pair = (booking[0],booking[1])
        return booking_pair 
        
    
    def DayBookingsCheck(self,day='today'):
        ''' day can be 'today' 'tomorrow' or 'YYYY-MM-DD' '''
        if day=='today':
            dateString = datetime.datetime.today() + datetime.timedelta(days=0)
            dateString = dateString.strftime('%Y-%m-%d')
        elif day=='tomorrow':
            dateString = datetime.datetime.today() + datetime.timedelta(days=1)
            dateString = dateString.strftime('%Y-%m-%d')
        else:
            dateString = day
            try:
                time.strptime(dateString, "%Y-%m-%d")
            except ValueError:
                #if error in date, default to years in the future hack
                dateString = '2222-01-01'
            

        
        commands = {'action':'getrunningsheet',
                    'noheaders':'true',
                    'day':dateString,
                    'plateformid':2,
                    'apikey':self.apiKey}
       
        bookings = self.Action(commands)
        bookings = string.split(bookings,'\n')[1:-1]
    
        report = []    
        
        for booking in bookings:
            booking = string.split(booking,',')
            name = string.split(booking[4][1:-1],' ')
            try:
                name = name[1] + ' ' + name[0]        
            except:
                name = name[0]
            startTime = booking[1][1:-1]
            endTime = booking[2][1:-1]
            report.append((name,booking[3][1:-1],startTime,endTime))
        
        #Make human readable and sort on name
        names = [x[0] for x in report]
        newnames = [x for n,x in enumerate(names) if x not in names[:n]]

        if dateString == '2222-01-01':
            toSend = 'Incorrect date format, should be YYYY-MM-DD'            
        else:
            toSend = 'Bookings for %s\n' % dateString
            for person in newnames:
                for thing in report:
                    if thing[0] == person:
                        toSend += '\t*%s* is on %s between %s and %s\n' % (person,thing[1],thing[2],thing[3])
        
        return toSend    

    def CurrentBookingsCheck(self):        
        h = int(datetime.datetime.now().hour)
        dateString = datetime.datetime.today() + datetime.timedelta(days=0)
        dateString = dateString.strftime('%Y-%m-%d')
        
        commands = {'action':'getrunningsheet',
                    'noheaders':'true',
                    'day':dateString,
                    'plateformid':2,
                    'apikey':self.apiKey}
       
        bookings = self.Action(commands)    
        bookings = string.split(bookings,'\n')[1:-1]
        
        
        report = []  
        
        for booking in bookings:
            booking = string.split(booking,',')
            name = string.split(booking[4][1:-1],' ')
            try:            
                name = name[1] + ' ' + name[0]                    
            except:
                name = name[0]
            startTime = booking[1][1:-1]
            endTime = booking[2][1:-1]
            startHour = int(startTime[0:2])
            endHour = int(endTime[0:2])
            if startHour > 12 and endHour < startHour:
                endHour = 24
            if endHour < startHour and startHour < 12:
                startHour = 0

            if h >= startHour and h < endHour:
                report.append((name,booking[3][1:-1],startTime,endTime))    
                    

        if len(report) > 0:
            names = [x[0] for x in report]
            newnames = [x for n,x in enumerate(names) if x not in names[:n]]
    
            toSend = 'Who\'s in the facility right now: \n'
            for person in newnames:
                for thing in report:
                    if thing[0] == person:
                        toSend += '\t*%s* is on %s between %s and %s\n' % (person,thing[1],thing[2],thing[3])        
        else:
            toSend = 'Would you believe there are no current bookings?'
        
        
        return toSend


    def Action(self,command):
        ''' perform PPMS query action, pass commands as a dict, output will
        be raw output from ppms api, will need formatting'''
        data = urllib.urlencode(command)
        r = urllib2.Request(self.ppms,data)
        response = urllib2.urlopen(r)
        time.sleep(2)
        #print('pinging ppms, %s' % datetime.datetime.now().strftime('%H:%M:%S'))
        output = response.read()
        return output

    
    def getTimeIn_N_Minutes(self,N):       
        new = time.time() + (N * 60)
        a  = time.ctime(new)
        a = string.split(a,' ')
        t = a[3][:-3] 
        
        return t
    
    def getReportForNDays(self,nDays):
        report = []            
        for N in range(nDays):        
            #time.sleep(0.1)
            dateString = datetime.datetime.today() + datetime.timedelta(days=N)
            dateString = dateString.strftime('%Y-%m-%d')
            commands = {'action':'getrunningsheet',
                    'noheaders':'false',
                    'day':dateString,
                    'plateformid':2,
                    'apikey':self.apiKey}
           # print dateString
           # print "Minute = %s" % datetime.datetime.now().minute
                      
            bookings = self.Action(commands)
           # print bookings  
            bookings = string.split(bookings,'\n')[1:-1]
            
        
            for booking in bookings:
                booking = string.split(booking,',')
                try:
                    name = string.split(booking[4][1:-1],' ')
                    name = name[1] + ' ' + name[0]        
                except:
                    name = name[0]
                startTime = booking[1][1:-1]
                endTime = booking[2][1:-1]
                report.append((name,booking[3][1:-1],startTime,endTime,dateString))
       
            
        return report

    def writeReportToFile(self,report):
        f = open('data/weeksBookings.list','w')
        f.write(str(report))
        f.close()
        self.bookingsInFile = report
    
    def checkBookingsAgainstFile(self,report):
        f = open('data/weeksBookings.list','r')
        bookingsOnFile = eval(f.read())
        f.close()
        toSend = ''
        
        if len(report) > len(bookingsOnFile):
            newBookings = [x for x in report if x not in bookingsOnFile]
            self.writeReportToFile(report)
            toSend = "New Booking(s)!\n\t"
            for new in newBookings:
                toSend = toSend + "*%s* booked on %s on %s\n\t" % (new[0],new[1],new[4])              
                try:
                    #CHANGE TO LAST BOOOKED TIME, RATHER THAN TOTAL EXPERIENCE
                    userXP = self.getXPonSystem(self.userDirectory[new[0]],self.systemIDs[new[1]])
                    toSend = toSend + "%s has %s hours experience\n\t" % (new[0],userXP)
                    if userXP < 10:
                        toSend = toSend + "*New user warning*\n\t"
                except:
                    toSend = toSend + ' - something wrong with experience lookup\n'
                
        if len(report) < len(bookingsOnFile):
            yesterday = datetime.datetime.today() + datetime.timedelta(days=-1)
            yesterday = yesterday.strftime('%Y-%m-%d')           
            
            deletedBookings = [x for x in bookingsOnFile if x not in report]
            self.writeReportToFile(report)
            prefix = "Cancelled Booking(s)!\n\t"
            toSend = ''
            for deleted in deletedBookings:
                if deleted[4] != yesterday:
                    toSend = toSend + "*%s* no longer booked on %s on %s\n" % (deleted[0],deleted[1],deleted[4])     
            if len(toSend)>0:
                toSend = prefix + toSend
            else:
                toSend = ''
                    
        self.bookingsInFile = report #keep up to date every check
        
        
        return toSend
    
    def getSystemNameInString(self,message):
        systemFromString = ''
        for sys in self.systemNames:            
            if re.match(r'.*'+sys+'.*',message):
                systemFromString = self.systemNames[sys]             
        if systemFromString=='':
            pass
        
        return systemFromString
    
    def getSystemAvailability(self,system,when='week'):
   
        f = open('data/weeksBookings.list','r')
        bookingsOnFile = eval(f.read())
        f.close()
        weekdays = {0:'Monday',1:'Tuesday',2:'Wednesday',3:'Thursday',4:'Friday',5:'Saturday',6:'Sunday'}
        daysOfWeek = {'monday':0,'tuesday':1,'wednesday':2,'thursday':3,'friday':4,'saturday':5,'sunday':6}
        today= datetime.datetime.today().weekday()
        timeIcareAbout = range(9,17)
        returnMessage = ''    
        mon = []
        tues = []
        wed = []
        thurs = []
        fri = []
        sat =[]
        sun = []
        week = [mon,tues,wed,thurs,fri,sat,sun]
       
    
        if when != 'week' and when!='today' and when!='tomorrow':
            when = daysOfWeek[when]
        
        for session in bookingsOnFile:
            u,sys,start,end,date = session
            if sys == system:
                y,m,d = str.split(date,'-')
                day = datetime.date(int(y),int(m),int(d)).weekday()
                start,end = int(start[0:2]),int(end[0:2])
                if end<start and start < 12:
                    start = 0
                if end<start and start > 12:
                    end = 24
                booked = range(start,end)
                
                for hour in booked:            
                    week[day].append(hour)
                #print session
    
        if when == 'week':
            for i,d in enumerate(week):
                if i >= today:
                    free = [x for x in timeIcareAbout if x not in d]
                    if len(free) == 0:
                        returnMessage += '%s unavailable %s\n' % (system,weekdays[i])
                    else:
                        if len(free)>=8:
                            returnMessage += '%s available *all day* %s\n' % (system,weekdays[i])
                        else:                
                            returnMessage += '%s available %i hours %s.\nFree for the hours starting %s\n' % (system,len(free),weekdays[i],str(free)[1:-1])
            for i,d in enumerate(week):
                if i < today:        
                    free = [x for x in timeIcareAbout if x not in d]
                    if len(free) == 0:
                        returnMessage += '%s unavailable %s\n' % (system,weekdays[i])
                    else:
                        if len(free)>=8:
                            returnMessage += '%s available *all day* %s\n' % (system,weekdays[i])
                        else:                
                            returnMessage += '%s available %i hours %s.\nFree for the hours starting %s\n' % (system,len(free),weekdays[i],str(free)[1:-1])
            
        if when == 'today':
            d = week[today]
            free = [x for x in timeIcareAbout if x not in d]
            if len(free) == 0:
                returnMessage += '%s unavailable today\n' % system
            else:
                if len(free)>=8:
                    returnMessage += '%s available *all day* today (%s)\n' % (system,weekdays[today])
                else:                
                    returnMessage += '%s available %i hours today (%s).\nFree for the hours starting %s\n' % (system,len(free),weekdays[today],str(free)[1:-1])
                        
        if when == 'tomorrow':
            if today != 6:
                today += 1 #hack, will make explicit later
            else:
                today = 0
            d = week[today]
            free = [x for x in timeIcareAbout if x not in d]
            if len(free) == 0:
                returnMessage += '%s unvailable tomorrow (%s)\n' % (system,today)
            else:
                if len(free)>=8:
                    returnMessage += '%s available *all day* tomorrow (%s)\n' % (system,weekdays[today])
                else:                
                    returnMessage += '%s available %i hours tomorrow (%s).\nFree for the hours starting %s\n' % (system,len(free),weekdays[today],str(free)[1:-1])
    
        if when in range(0,7):
            today = when #again, hack        
            d = week[today]
            free = [x for x in timeIcareAbout if x not in d]
            if len(free) == 0:
                returnMessage += '%s unvailable %s\n' % (system,weekdays[today])
            else:
                if len(free)>=8:
                    returnMessage += '%s available *all day* %s\n' % (system,weekdays[today])
                else:                
                    returnMessage += '%s available %i hours %s.\nFree for the hours starting %s\n' % (system,len(free),weekdays[today],str(free)[1:-1])
    
        return returnMessage


