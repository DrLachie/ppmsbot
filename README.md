PPMSBot - An interface for PPMS and Slack
====================

PPMSBot is a work in progress and is provided as is. Note that it's specifically tailored for use in the Center for Dynamic Imaging at the Walter and Eliza Hall Institute.


#Installation
* Currently running in Python 2.7 
* Requires Pyyaml
* Requires old version of slackclient (1.0.2)

#Keys and URLs

## Put the following into the PPMSBot_Keys.dict file:
* PUMAPI Key
* PPMS API2 Key
* Slack BotToken 
* PUMAPI URL
* PPMS API2URL

The API keys and URLS can be obtained from your PPMS instance superadministrator. 
The BotToken is generated in your slack team's administrator dashboard under the custom integrations tab. 
** Do not share these keys or the bottoken publicly **
Also note that the bot will only have the level of authority as the ppms user whom the keys are generated for (I think).  

##Channels to send to
By default PPMSBot sends messages to either MyPrivateBotChannel or the AdminChannel. These can both be altered in the ppmsBot.py file 

##Admins
PPMSBot also only responds to named users in the admins list found in ppmsBot.py

# Adding Users
Before running for the first time. Delete the user.dict file (which has a list of users of my ppms instance, not yours). The bot will then create a new file with a list of your users. Note that this can take a very long time as each user requires two api calls and these are limited to one every two seconds. PPMSBot will check for new users every night at one minute past middnight. 

#Adding instruments 
PPMSBot can read the instruments names from your PPMS instance, but you will need to add shorthand names and nicknames for the instruments to be able to interface with them via slack. The files systemFullNames.dict is used for querying system availability, while systemNicknames.dict is used for reporting incidents. 

#Adding functions
To add functionality, define a new function in ppmsBot.py that takes as input the slack Message object (IM) and the ppmsObject. Ideally it should also return a string to be sent as a message to slack but can do other things as well. 

Define a regular expression that will trigger the function and add the regex and function name to the "resp" list in the interpretMessage function. 

#Custom actions based on bookings
You can use the output from PPMSBot to trigger further actions. This is done essentially the same way but the regex is used on the output from the bot, rather than the user input from slack. 

#Scheduled Functions
If PPMSBot doesn't see any messages on slack, it can be told to do other jobs. The obvious one is that every five minutes it will check for new bookings in the next N days.
I also suggest defining a boolean to stop scheduled functions running continuously for the whole minute that the IF statement is true. haveDoneThis and haveRunUserCheck are examples of this. 

#Reminders 
Limited functionality at this point. But you can write to the reminder.list file with scheduled messages. Format of a reminder is: 
[hour,minute,day*,message,channel,True] 
Day is optional, Monday = 0, Sunday = 6
You can add or view reminders by saying: "ppmsbot remind me to "do something" at hh:mm and a day (today, everyday, or a day of the week)."  
You can delete (or view) reminders by saying: "Delete reminder".   
Note all reminders repeat weekly, or everyday. So you'll need to delete unuseful ones after they've served their purpose.   
Also note "remind me" only works in the ppmsbot channel, or "admin_chat" in the current case. 


#Implementing other PPMS functions
I've tried to keep the slack functions separate from the PPMS ones. If you have a new call to make to PPMS then I suggest putting it into ppmsQuery.py and specifically making it a function of the ppmsQuery class. Put the payload into a dict object and then use self.Action(dict) to send the API the POST request and retrun the raw output from the API. It will also pause for 2 seconds to avoid hitting the API limits and slowing everything down. 








